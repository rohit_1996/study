package com.Javalearn.Study;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {
	
	@Autowired
	UserRepository ur;
	
	public UserRecords saveuser(UserRecords usr) {
		return ur.save(usr);
	}
	
	
	
}
